using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Newtonsoft.Json.Linq;

using UAssetAPI;

namespace CustomSlots
{
    static class AssetUtils
    {
        public static UAsset ReadFileAsAsset(string assetPath) 
        {
            var version = UE4Version.VER_UE4_24;
            return new UAsset(assetPath, version);
        }
        public static JObject ReadFileAsJson(string assetPath)
        {
            // inefficient but shrug.
            return JObject.Parse(ReadFileAsAsset(assetPath).SerializeJson());
        }
        private static byte[] ReadFileAsBytes(string assetPath)
        {
            return File.ReadAllBytes(assetPath);
        }

        public static void ReplacePathHex(string assetPath, string newID)
        {
            // This is for textures, because UAssetAPI generates invalid files
            const int IDIndex = 0xED;
            var replacement = Encoding.ASCII.GetBytes(newID);

            byte[] assetBytes = ReadFileAsBytes(assetPath);

            for (int i = 0; i < replacement.Length; i++)
            {
                assetBytes[IDIndex+i] = replacement[i];
            }

            File.WriteAllBytes(assetPath, assetBytes);
        }

        private static void ReplacePathUAPI(string assetPath, string newID)
        {
            var searchPattern = @"/chr_body_.*?/";
            var replacement = $"/chr_body_{newID}/";
            var assetFile = ReadFileAsAsset(assetPath);

            foreach (var import in assetFile.Imports) 
            {
                if (import.ClassName.Equals(FName.FromString("Package(0)"))
                    && !import.ObjectName.Equals(FName.FromString("/Script/Engine(0)"))
                    && import.ObjectName.ToString().Contains("Character/Body/Cos/"))
                    {
                        var ogName = import.ObjectName.ToString().Replace("(0)", "");
                        var newName = Regex.Replace(import.ObjectName.ToString(), searchPattern, replacement);
                        import.ObjectName = FName.FromString(newName);

                        var searchFname = FString.FromString(ogName);
                        var nameIndex = assetFile.SearchNameReference(searchFname);
                        assetFile.SetNameReference(nameIndex, FString.FromString(newName.Replace("(0)", "")));
                    }
            }

            // Change the path of the file in the NameMap
            var nameMap = assetFile.GetNameMapIndexList();
            List<int> mapIndexes = new List<int>();

            for (int i = 0; i < nameMap.Count; i++)
            {
                if (Regex.IsMatch(nameMap[i].ToString(), searchPattern) && nameMap[i].ToString().Contains("Character/Body/Cos/"))
                {
                    mapIndexes.Add(i);
                }
            }

            if (mapIndexes.Count == 0)
            {
                CustomSlots.updateOutput("Warning, object/file name mismatch...");
            } else
            {
                foreach (var index in mapIndexes)
                {
                    var newMapName = Regex.Replace(assetFile.GetNameReference(index).ToString(), searchPattern, replacement);
                    assetFile.SetNameReference(index, FString.FromString(newMapName));
                }
            }

            assetFile.Write(assetPath);
        }

        private static void CheckImports(string uassetFile, string gameFolder, string costumeFolder)
        {
            var assetFile = ReadFileAsAsset(uassetFile);
            var pathPrefix = new DirectoryInfo(Path.GetDirectoryName(uassetFile)).Parent.Parent.Parent.ToString();
            foreach (var import in assetFile.Imports)
            {
                if (import.ClassName.Equals(FName.FromString("Package(0)"))
                    && !import.ObjectName.Equals(FName.FromString("/Script/Engine(0)"))
                    && import.ObjectName.ToString().Contains("Character/Body/Cos"))
                {
                    var pakPath = import.ObjectName.ToString().Replace("(0)", "");
                    var pathSuffix = pakPath.Replace("/Game/Model/Character/Body/", "").Split("/");
                    var fileName = Path.Combine(pathPrefix, Path.Combine(pathSuffix));
                    
                    // Extracting missing materials/textures from the game files
                    if (!File.Exists(fileName+".uasset"))
                    {
                        PakUtils.ExtractFromPak(gameFolder, pakPath, costumeFolder);
                    }
                }
            }
        }

        public static void ReplacePaths(string costumeFolder, string gamePakFolder, Costume cos)
        {
            var defaultCostumesPath = Path.Combine(costumeFolder, "extracted", "StarlitSeason", "Content", "Model", "Character", "Body", "Cos");

            List<string> checkedFiles = new List<string>();
            var allFiles = Directory.GetFiles(costumeFolder, "*.uasset", SearchOption.AllDirectories);
            foreach (var file in allFiles)
            {
                CheckImports(file, gamePakFolder, Path.Combine(costumeFolder, "extracted"));
                checkedFiles.Append(file);
            }

            var BodyGmPhysicsAsset = string.Empty;
            allFiles = Directory.GetFiles(costumeFolder, "*.uasset", SearchOption.AllDirectories);
            foreach (var file in allFiles)
            {
                if (!checkedFiles.Contains(file))
                {
                    CheckImports(file, gamePakFolder, Path.Combine(costumeFolder, "extracted"));
                }
            }

            var baseDirectories = Directory.GetDirectories(defaultCostumesPath);
            foreach (var dir in baseDirectories)
            {
                var newFolder = Path.GetDirectoryName(dir);
                CopyAll(dir, Path.Combine(newFolder, $"chr_body_{cos.ID}"));
                Directory.Delete(dir, true);
            }

            allFiles = Directory.GetFiles(costumeFolder, "*.uasset", SearchOption.AllDirectories);
            foreach (var file in allFiles)
            {
                // CustomSlots.updateOutput(file);
                var folderName = new DirectoryInfo(Path.GetDirectoryName(file)).Name;
                //if (folderName == "Texture")
                if ( folderName == "Texture")
                {
                    ReplacePathHex(file, cos.ID);
                }
                else if ( folderName == "UnitDress")
                {
                    ReplaceWidgetPathHex(file, cos.IconID);
                }
                else
                {
                    ReplacePathUAPI(file, cos.ID);
                }
            }
        }

        public static void ReplaceWidgetPathHex(string assetPath, string newID)
            {
                //Would really appreciate if there's someone who can refactor/optimize the code
                // Where the c in WDT_cosxxx starts
                const int IDIndexStart = 0xEC;
                //const int IDIndexEnd = 0xF3;
                var replacement = Encoding.ASCII.GetBytes(newID.Replace("WDT_",""));
                byte[] assetBytes = ReadFileAsBytes(assetPath);

                var bytesToString = Encoding.UTF8.GetString(assetBytes);
                byte[] patternToBeReplaced = new byte[7];

                //Replace first occurence
                for (int i = 0; i < replacement.Length; i++)
                {
                    patternToBeReplaced[i] = assetBytes[IDIndexStart+i];
                    assetBytes[IDIndexStart+i] = replacement[i];
                }
                //Loop to find the second occurence since the position isn't fixed
                //Step up the starting search index since we know that the earlier bytes aren't the IconID hexs + save looping time
                int startIndexOf2ndIconID = IndexOfPattern(assetBytes,patternToBeReplaced,IDIndexStart); 
                //Replace second occurence
                for (int i = 0; i < replacement.Length; i++)
                {
                    assetBytes[startIndexOf2ndIconID+i] = replacement[i];
                }

                File.WriteAllBytes(assetPath, assetBytes);
            }

        //Snippet of Code from StackOverFlow, credit to CodeHack
        public static int IndexOfPattern(byte[] src, byte[] pattern, int startInd)
        {
            for (int x = 0 + startInd; x < src.Length; x++)
            {
                byte currentValue = src[x];
                if (currentValue != pattern[0]) continue;

                bool match = false;

                for (int y = 0; y < pattern.Length; y++)
                {
                    byte tempValue = src[x + y];
                    if (tempValue != pattern[y])
                    {
                        match = false;
                        break;
                    }

                    match = true;
                }

                if (match)
                    return x;
            }

            return -1;
        }


        public static void AddEntries(string dataTablePath, List<JObject> jObjList, List<string> newNames)
        {
            var dataTable = ReadFileAsJson(dataTablePath+".uasset");
            var newArray = (JArray) dataTable["Exports"][0]["Table"]["Data"];
            var newNameMap = (JArray) dataTable["NameMap"];

            // Workaround because UAssetAPI breaks WidthMode
            //Apparently WidthMode is broken on all costumes actually
            if (dataTablePath.Contains("CostumeModelDataTable")) 
            {
                foreach (var item in newArray)
                {
                    // Work around for cos104A
                    if (item["Name"].ToString() == "cos104A(0)")
                    {
                        var wfix = JObject.Parse(jsonTemplates.WidthModeFix);
                        item["Value"][7]["Value"][15] = wfix;
                    }
                    
                    // Work around for costumes with texture replacement
                    // (Costumes with "_chr" imply color variant)
                    if (item.Contains("_chr")) {
                        var texturePathReplace = item["Name"].ToString().Replace("(0)", "").Split('_').First();
                        var regexTexturePathReplace = new Regex(texturePathReplace.Remove(texturePathReplace.Length-1));
                        item["Value"][3]["Value"][0]["Value"] 
                            = regexTexturePathReplace.Replace(
                                item["Value"][3]["Value"][0]["Value"].ToString().Replace("(0)", ""),texturePathReplace,1
                                );

                    }
                    // Work around for Costumes with mesh variation (testing cos001)
                    /*if (item["Name"].ToString() == "cos104A(0)")
                    {

                    }*/
                }
            }

            foreach (var jObj in jObjList)
            {
                if (dataTablePath.Contains("MD_CostumeModel"))
                {
                    var cleanItem = jObj["Value"][2]["Value"].ToString().Replace("(0)", "");
                    if (!valueExists(newNameMap, cleanItem))
                    {
                        if (!cleanItem.Contains(jObj["Value"][1]["Value"].ToString().Replace("(0)", "")))
                            jObj["Value"][2]["Value"] = "None(0)";
                    }
                }

                newArray.Add(jObj);
            }

            dataTable["Exports"][0]["Table"]["Data"] = newArray;

            foreach (var item in newNames)
            {
                var cleanItem = item.Replace("(0)", "");
                if (!valueExists(newNameMap, cleanItem))
                {
                    newNameMap.Add(cleanItem);
                }
            }

            var sortedNameMap = new JArray(newNameMap.OrderBy(obj => (string)obj));
            dataTable["NameMap"] = sortedNameMap;

            var jsonDeserializedAsset = UAsset.DeserializeJson(dataTable.ToString());
            jsonDeserializedAsset.Write(dataTablePath+".uasset");
        }

        private static bool valueExists(JArray jArr, string newItem) 
        {
            foreach (var item in jArr)
            {
                if (item.ToString() == newItem)
                {
                    return true;
                }
            }

            return false;
        }

        public static void CopyAll(string source, string target)
        {
            var dsource = new DirectoryInfo(source);
            var dtarget = new DirectoryInfo(target);
            if (dsource.FullName.ToLower() == dtarget.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(dtarget.FullName) == false)
            {
                Directory.CreateDirectory(dtarget.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in dsource.GetFiles())
            {
                fi.CopyTo(Path.Combine(dtarget.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in dsource.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    dtarget.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir.ToString(), nextTargetSubDir.ToString());
            }
        }
    }
}