using System.Collections.Generic;

namespace CustomSlots
{
    public class Costume
    {
        public string FileName;
        public string ID = string.Empty;
        public string BaseID;
        public string Name;
        public string Description;
        public int SortNum = -1;
        public string BodyMesh = string.Empty;
        public string BodyGmPhysicsAsset = string.Empty;

        //Used to refer the ID of the icon of the costume from AssetRegistry
        public string IconID = string.Empty;

        //Used to check if the costume is acceptable for multiple mesh variants
        public bool eligibleForMeshVariant = false;

        ////Used to check if the costume is packed with a custom accessory model
        public bool ToggleAbleAccessories = false;
        
        //Used to store accesories the costume is packed with
        public List<AttachAccessory> accessories = new List<AttachAccessory>();
        public Costume(string fileName, string name, string description)
        {
            FileName = fileName;
            // ID = iD;
            // BaseID = baseID;
            Name = name;
            Description = description;
            // SortNum = sortNum;
        }
    }
}
