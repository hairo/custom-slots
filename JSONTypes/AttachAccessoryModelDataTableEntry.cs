using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class AttachAccessoryModelDataTableEntry
    {
        public string Name;
        public string MeshData;
        public string AttachSkeleton;
        public bool bTurnOffEyebrowsAndEyelashes;
        public bool bCastShadow;
        public bool bPhysicsBodyHit;
        private string AnimBlueprint;
        private string GmPhysicsAsset;
        private int TranslucentSortPriority;
        private string ReplacementMaterianName1;
        private string ReplacementMaterianName2;
        private string ReplacementMaterianName3;
        private string ReplacementMaterianName4;
        private string ReplacementMaterianName5;
        private string ReplacementMaterianName6;
        private string TextureData1;
        private string TargetTextureName1;
        private string TextureData2;
        private string TargetTextureName2;
        private bool bDefaultVisibility;
        private int GimmickChannel;
        private string AnimData;
        public AttachAccessoryModelDataTableEntry(string inName, string inMeshData, string inAttachSkeleton, 
                                                  bool inbTurnOffEyebrowsAndEyelashes, bool inbCastShadow,
                                                  bool inbPhysicsBodyHit)
        {
            Name = inName+"(0)";
            MeshData = "/Game/Model/Character/"+inMeshData+"(0)";
            AttachSkeleton = inAttachSkeleton; // EAttachAccessorySkeleton::BODY(0) or EAttachAccessorySkeleton::FACE(0) or EAttachAccessorySkeleton::HAIR(0)
            bTurnOffEyebrowsAndEyelashes = inbTurnOffEyebrowsAndEyelashes;
            bCastShadow = inbCastShadow;
            bPhysicsBodyHit = inbPhysicsBodyHit;
            // Not used for now
            AnimBlueprint = "None(0)";
            GmPhysicsAsset = "None(0)";
            TranslucentSortPriority = 0; // 0 or 1
            ReplacementMaterianName1 = null;
            ReplacementMaterianName2 = null;
            ReplacementMaterianName3 = null;
            ReplacementMaterianName4 = null;
            ReplacementMaterianName5 = null;
            ReplacementMaterianName6 = null;
            TextureData1 = "None(0)";
            TargetTextureName1 = null;
            TextureData2 = "None(0)";
            TargetTextureName2 = null;
            bDefaultVisibility = true;
            GimmickChannel = 0; // 0 or 1
            AnimData = "None(0)";
        }

        public JObject ToJson()
        {
            var templ = JObject.Parse(jsonTemplates.AttachAccessoryModelDataTableItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = MeshData;
            templ["Value"][1]["Value"] = AttachSkeleton;
            templ["Value"][2]["Value"] = AnimBlueprint;
            templ["Value"][3]["Value"] = GmPhysicsAsset;
            templ["Value"][4]["Value"] = bTurnOffEyebrowsAndEyelashes;
            templ["Value"][5]["Value"] = bTurnOffEyebrowsAndEyelashes;
            templ["Value"][6]["Value"] = bTurnOffEyebrowsAndEyelashes;
            templ["Value"][7]["Value"] = bTurnOffEyebrowsAndEyelashes;
            templ["Value"][8]["Value"] = bCastShadow;
            templ["Value"][9]["Value"] = TranslucentSortPriority;
            templ["Value"][10]["Value"] = bPhysicsBodyHit;
            templ["Value"][11]["Value"] = ReplacementMaterianName1;
            templ["Value"][12]["Value"] = ReplacementMaterianName2;
            templ["Value"][13]["Value"] = ReplacementMaterianName3;
            templ["Value"][14]["Value"] = ReplacementMaterianName4;
            templ["Value"][15]["Value"] = ReplacementMaterianName5;
            templ["Value"][16]["Value"] = ReplacementMaterianName6;
            templ["Value"][17]["Value"][0]["Value"] = TextureData1;
            templ["Value"][17]["Value"][1]["Value"] = TargetTextureName1;
            templ["Value"][18]["Value"][0]["Value"] = TextureData2;
            templ["Value"][18]["Value"][1]["Value"] = TargetTextureName2;
            templ["Value"][19]["Value"] = bDefaultVisibility;
            templ["Value"][20]["Value"] = GimmickChannel;
            templ["Value"][21]["Value"] = AnimData;

            return templ;
        }
    }
}