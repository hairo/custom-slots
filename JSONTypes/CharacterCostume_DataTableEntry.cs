using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class CharacterCostume_DataTableEntry
    {
        public string Name;
        public string CharacterBaseID;
        public string CostumeID;
        private string SkeletalAccessoryID1;
        private string SkeletalAccessoryID2;
        private string SkeletalAccessoryID3;
        private string AttachAccessoryID1;
        private string AccessoryAttachBone1;
        private float[] AccessoryAttachPos1;
        private float[]  AccessoryAttachRot1;
        private float AccessoryScale1;
        private string AttachAccessoryID2;
        private string AccessoryAttachBone2;
        private float[] AccessoryAttachPos2;
        private float[]  AccessoryAttachRot2;
        private float AccessoryScale2;
        private string AttachAccessoryID3;
        private string AccessoryAttachBone3;
        private float[] AccessoryAttachPos3;
        private float[]  AccessoryAttachRot3;
        private float AccessoryScale3;
        private string AttachAccessoryID4;
        private string AccessoryAttachBone4;
        private float[] AccessoryAttachPos4;
        private float[]  AccessoryAttachRot4;
        private float AccessoryScale4;
        private string AttachAccessoryID5;
        private string AccessoryAttachBone5;
        private float[] AccessoryAttachPos5;
        private float[]  AccessoryAttachRot5;
        private float AccessoryScale5;
        private string AttachAccessoryID6;
        private string AccessoryAttachBone6;
        private float[] AccessoryAttachPos6;
        private float[]  AccessoryAttachRot6;
        private float AccessoryScale6;
        private string HideHairBone1;
        private string HideHairBone2;
        private string HideHairBone3;
        public CharacterCostume_DataTableEntry(string inName, string inCharacterBaseID, string inCostumeID)
        {
            Name = inName+"(0)";
            CharacterBaseID = inCharacterBaseID+"(0)";
            CostumeID = inCostumeID+"(0)";
            // not used for now
            SkeletalAccessoryID1 = "None(0)";
            SkeletalAccessoryID2 = "None(0)";
            SkeletalAccessoryID3 = "None(0)";

            AttachAccessoryID1 = "None(0)";
            AccessoryAttachBone1 = "None(0)";
            AccessoryAttachPos1 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot1 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale1 = 1.0F;

            AttachAccessoryID2 = "None(0)";
            AccessoryAttachBone2 = "None(0)";
            AccessoryAttachPos2 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot2 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale2 = 1.0F;

            AttachAccessoryID3 = "None(0)";
            AccessoryAttachBone3 = "None(0)";
            AccessoryAttachPos3 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot3 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale3 = 1.0F;

            AttachAccessoryID4 = "None(0)";
            AccessoryAttachBone4 = "None(0)";
            AccessoryAttachPos4 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot4 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale4 = 1.0F;

            AttachAccessoryID5 = "None(0)";
            AccessoryAttachBone5 = "None(0)";
            AccessoryAttachPos5 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot5 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale5 = 1.0F;

            AttachAccessoryID6 = "None(0)";
            AccessoryAttachBone6 = "None(0)";
            AccessoryAttachPos6 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryAttachRot6 = new float[3] {0, 0, 0}; // X, Y, Z
            AccessoryScale6 = 1.0F;

            HideHairBone1 = "None(0)";
            HideHairBone2 = "None(0)";
            HideHairBone3 = "None(0)";
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.CharacterCostume_DataTableItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = CharacterBaseID;
            templ["Value"][1]["Value"] = CostumeID;
            templ["Value"][2]["Value"] = SkeletalAccessoryID1;
            templ["Value"][3]["Value"] = SkeletalAccessoryID2;
            templ["Value"][4]["Value"] = SkeletalAccessoryID3;

            templ["Value"][5]["Value"] = AttachAccessoryID1;
            templ["Value"][6]["Value"] = AccessoryAttachBone1;
            templ["Value"][7]["Value"][0]["Value"]["X"] = AccessoryAttachPos1[0];
            templ["Value"][7]["Value"][0]["Value"]["Y"] = AccessoryAttachPos1[1];
            templ["Value"][7]["Value"][0]["Value"]["Z"] = AccessoryAttachPos1[2];
            templ["Value"][8]["Value"][0]["Value"]["X"] = AccessoryAttachRot1[0];
            templ["Value"][8]["Value"][0]["Value"]["Y"] = AccessoryAttachRot1[1];
            templ["Value"][8]["Value"][0]["Value"]["Z"] = AccessoryAttachRot1[2];
            templ["Value"][9]["Value"] = AccessoryScale1;

            templ["Value"][10]["Value"] = AttachAccessoryID2;
            templ["Value"][11]["Value"] = AccessoryAttachBone2;
            templ["Value"][12]["Value"][0]["Value"]["X"] = AccessoryAttachPos2[0];
            templ["Value"][12]["Value"][0]["Value"]["Y"] = AccessoryAttachPos2[1];
            templ["Value"][12]["Value"][0]["Value"]["Z"] = AccessoryAttachPos2[2];
            templ["Value"][13]["Value"][0]["Value"]["X"] = AccessoryAttachRot2[0];
            templ["Value"][13]["Value"][0]["Value"]["Y"] = AccessoryAttachRot2[1];
            templ["Value"][13]["Value"][0]["Value"]["Z"] = AccessoryAttachRot2[2];
            templ["Value"][14]["Value"] = AccessoryScale2;

            templ["Value"][15]["Value"] = AttachAccessoryID3;
            templ["Value"][16]["Value"] = AccessoryAttachBone3;
            templ["Value"][17]["Value"][0]["Value"]["X"] = AccessoryAttachPos3[0];
            templ["Value"][17]["Value"][0]["Value"]["Y"] = AccessoryAttachPos3[1];
            templ["Value"][17]["Value"][0]["Value"]["Z"] = AccessoryAttachPos3[2];
            templ["Value"][18]["Value"][0]["Value"]["X"] = AccessoryAttachRot3[0];
            templ["Value"][18]["Value"][0]["Value"]["Y"] = AccessoryAttachRot3[1];
            templ["Value"][18]["Value"][0]["Value"]["Z"] = AccessoryAttachRot3[2];
            templ["Value"][19]["Value"] = AccessoryScale3;

            templ["Value"][20]["Value"] = AttachAccessoryID4;
            templ["Value"][21]["Value"] = AccessoryAttachBone4;
            templ["Value"][22]["Value"][0]["Value"]["X"] = AccessoryAttachPos4[0];
            templ["Value"][22]["Value"][0]["Value"]["Y"] = AccessoryAttachPos4[1];
            templ["Value"][22]["Value"][0]["Value"]["Z"] = AccessoryAttachPos4[2];
            templ["Value"][23]["Value"][0]["Value"]["X"] = AccessoryAttachRot4[0];
            templ["Value"][23]["Value"][0]["Value"]["Y"] = AccessoryAttachRot4[1];
            templ["Value"][23]["Value"][0]["Value"]["Z"] = AccessoryAttachRot4[2];
            templ["Value"][24]["Value"] = AccessoryScale4;

            templ["Value"][25]["Value"] = AttachAccessoryID5;
            templ["Value"][26]["Value"] = AccessoryAttachBone5;
            templ["Value"][27]["Value"][0]["Value"]["X"] = AccessoryAttachPos5[0];
            templ["Value"][27]["Value"][0]["Value"]["Y"] = AccessoryAttachPos5[1];
            templ["Value"][27]["Value"][0]["Value"]["Z"] = AccessoryAttachPos5[2];
            templ["Value"][28]["Value"][0]["Value"]["X"] = AccessoryAttachRot5[0];
            templ["Value"][28]["Value"][0]["Value"]["Y"] = AccessoryAttachRot5[1];
            templ["Value"][28]["Value"][0]["Value"]["Z"] = AccessoryAttachRot5[2];
            templ["Value"][29]["Value"] = AccessoryScale5;

            templ["Value"][30]["Value"] = AttachAccessoryID6;
            templ["Value"][31]["Value"] = AccessoryAttachBone6;
            templ["Value"][32]["Value"][0]["Value"]["X"] = AccessoryAttachPos6[0];
            templ["Value"][32]["Value"][0]["Value"]["Y"] = AccessoryAttachPos6[1];
            templ["Value"][32]["Value"][0]["Value"]["Z"] = AccessoryAttachPos6[2];
            templ["Value"][33]["Value"][0]["Value"]["X"] = AccessoryAttachRot6[0];
            templ["Value"][33]["Value"][0]["Value"]["Y"] = AccessoryAttachRot6[1];
            templ["Value"][33]["Value"][0]["Value"]["Z"] = AccessoryAttachRot6[2];
            templ["Value"][34]["Value"] = AccessoryScale6;

            templ["Value"][35]["Value"] = HideHairBone1;
            templ["Value"][36]["Value"] = HideHairBone2;
            templ["Value"][37]["Value"] = HideHairBone3;

            return templ;
        }
    }
}