using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class MD_CostumeModelEntry
    {
        public string Name;
        public string CostumeID;
        public string CostumeID2;
        private string DisplayName;
        private string CenterCostumeID;
        private string CenterCostumeID2;
        public MD_CostumeModelEntry(string inName, string inCostumeID2)
        {
            Name = inName+"(0)";
            CostumeID = inName+"(0)";
            CostumeID2 = inCostumeID2;
            // Not used for now
            DisplayName = "";
            CenterCostumeID = "None(0)";
            CenterCostumeID2 = "None(0)";
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.MD_CostumeModelItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = DisplayName;
            templ["Value"][1]["Value"] = CostumeID;
            templ["Value"][2]["Value"] = CostumeID2;
            templ["Value"][3]["Value"] = CenterCostumeID;
            templ["Value"][4]["Value"] = CenterCostumeID2;

            return templ;
        }
    }
}