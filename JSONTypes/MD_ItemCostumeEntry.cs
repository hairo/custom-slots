using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class MD_ItemCostumeEntry
    {
        public string Name;
        public int Sort;
        public string IconID;
        public string CostumeName;
        public string CostumeText;
        public MD_ItemCostumeEntry(string inName, int inSort, string inIconID, string inCostumeName, string inCostumeText)
        {
            Name = inName+"(0)";
            Sort = inSort;
            IconID = inIconID;
            CostumeName = inCostumeName;
            CostumeText = inCostumeText;
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.MD_ItemCostumeItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = Sort;
            templ["Value"][1]["Value"] = IconID;
            templ["Value"][3]["Value"] = CostumeName;
            templ["Value"][5]["Value"] = CostumeText;

            return templ;
        }
    }
}