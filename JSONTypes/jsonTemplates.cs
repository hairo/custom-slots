
namespace CustomSlots
{
        static class jsonTemplates
    {
        public const string MD_ItemCostumeItem = @"{
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'MD_ItemCostume(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': '',
                'DuplicationIndex': 0,
                'Value': [
                {
                    '$type': 'UAssetAPI.PropertyTypes.IntPropertyData, UAssetAPI',
                    'Name': 'Sort(0)',
                    'DuplicationIndex': 0,
                    'Value': 0
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'IconID(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                    'EnumType': 'EItemCategory(0)',
                    'Name': 'Category(0)',
                    'DuplicationIndex': 0,
                    'Value': 'EItemCategory::COSTUME(0)'
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'CostumeName(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                    'EnumType': 'EItemCostumeGroup(0)',
                    'Name': 'CostumeGroup(0)',
                    'DuplicationIndex': 0,
                    'Value': 'EItemCostumeGroup::NONE(0)'
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'CostumeText(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'CopyrightText(0)',
                    'DuplicationIndex': 0,
                    'Value': null
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                    'EnumType': 'EItemDetailCategory(0)',
                    'Name': 'DetailCategory(0)',
                    'DuplicationIndex': 0,
                    'Value': 'EItemDetailCategory::DLC_COSTUME(0)'
                }
                ]
            }";

        public const string MD_DLCUnlockConditionSteamItem = @"{
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'MD_DLCUnlockConditionSteam(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': '',
                'DuplicationIndex': 0,
                'Value': [
                {
                    '$type': 'UAssetAPI.PropertyTypes.UInt32PropertyData, UAssetAPI',
                    'Name': 'AppID(0)',
                    'DuplicationIndex': 0,
                    'Value': 0
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'Next(0)',
                    'DuplicationIndex': 0,
                    'Value': 'None(0)'
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                    'EnumType': 'EDLCUnlockLogicalOperator(0)',
                    'Name': 'Operator(0)',
                    'DuplicationIndex': 0,
                    'Value': 'EDLCUnlockLogicalOperator::NONE(0)'
                }
                ]
            }";

        public const string MD_CostumeModelItem = @"{
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'MD_CostumeModel(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': '',
                'DuplicationIndex': 0,
                'Value': [
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'DisplayName(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'CostumeID(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'CostumeID2(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'CenterCostumeID(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'CenterCostumeID2(0)',
                    'DuplicationIndex': 0,
                    'Value': 'None(0)'
                }
                ]
            }";

        public const string CostumeModelDataTableItem = @"{
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'CostumeModelTableRow(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': '',
                'DuplicationIndex': 0,
                'Value': [
                {
                    '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                    'ID': 0,
                    'Name': 'BodyMesh(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                    'ID': 0,
                    'Name': 'BodyGmPhysicsAsset(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                    'ID': 0,
                    'Name': 'WearMesh(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                    'StructType': 'ReplacementTextureData(0)',
                    'SerializeNone': true,
                    'StructGUID': '00000000-0000-0000-0000-000000000000',
                    'Name': 'ReplacementTexture1(0)',
                    'DuplicationIndex': 0,
                    'Value': [
                    {
                        '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                        'ID': 0,
                        'Name': 'TextureData(0)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                        'Name': 'TargetTextureName(0)',
                        'DuplicationIndex': 0,
                        'Value': null
                    }
                    ]
                },
                {
                    '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                    'StructType': 'ReplacementTextureData(0)',
                    'SerializeNone': true,
                    'StructGUID': '00000000-0000-0000-0000-000000000000',
                    'Name': 'ReplacementTexture2(0)',
                    'DuplicationIndex': 0,
                    'Value': [
                    {
                        '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                        'ID': 0,
                        'Name': 'TextureData(0)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                        'Name': 'TargetTextureName(0)',
                        'DuplicationIndex': 0,
                        'Value': null
                    }
                    ]
                },
                {
                    '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                    'StructType': 'ReplacementTextureData(0)',
                    'SerializeNone': true,
                    'StructGUID': '00000000-0000-0000-0000-000000000000',
                    'Name': 'FaceReplacementTexture(0)',
                    'DuplicationIndex': 0,
                    'Value': [
                    {
                        '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                        'ID': 0,
                        'Name': 'TextureData(0)',
                        'DuplicationIndex': 0,
                        'Value': 'None(0)'
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                        'Name': 'TargetTextureName(0)',
                        'DuplicationIndex': 0,
                        'Value': null
                    }
                    ]
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'AccessoryReplacementMaterialInfo(0)',
                    'DuplicationIndex': 0,
                    'Value': null
                },
                {
                    '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                    'StructType': 'CostumeEffectData(0)',
                    'SerializeNone': true,
                    'StructGUID': '00000000-0000-0000-0000-000000000000',
                    'Name': 'EffectData(0)',
                    'DuplicationIndex': 0,
                    'Value': [
                    {
                        '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                        'ID': 0,
                        'Name': 'ParticleData(0)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.IntPropertyData, UAssetAPI',
                        'Name': 'GimmickChannel(0)',
                        'DuplicationIndex': 0,
                        'Value': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Duration(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                        'Name': 'bIsTrail(0)',
                        'DuplicationIndex': 0,
                        'Value': false
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                        'EnumType': 'EAttachAccessorySkeleton(0)',
                        'Name': 'AttachSkeleton(0)',
                        'DuplicationIndex': 0,
                        'Value': 'EAttachAccessorySkeleton::FACE(0)'
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                        'Name': 'AttachBone1(2)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                        'Name': 'AttachBone1(3)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot1_X(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot1_Y(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot1_Z(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                        'Name': 'AttachBone2(2)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                        'Name': 'AttachBone2(3)',
                        'DuplicationIndex': 0,
                        'Value': ''
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot2_X(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot2_Y(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'Rot2_Z(0)',
                        'DuplicationIndex': 0
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                        'EnumType': 'ETrailWidthMode(0)',
                        'Name': 'WidthMode(0)',
                        'DuplicationIndex': 0,
                        'Value': 'ETrailWidthMode_FromCentre(0)'
                    },
                    {
                        '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                        'Value': '+0',
                        'Name': 'WidthScale(0)',
                        'DuplicationIndex': 0
                    }
                    ]
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'HideBodyBone1(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                    'Name': 'HideBodyBone2(0)',
                    'DuplicationIndex': 0,
                    'Value': ''
                },
                {
                    '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                    'Name': 'bNeedToonSubsurfacePostProcess(0)',
                    'DuplicationIndex': 0,
                    'Value': false
                }
                ]
            }";

        public const string WidthModeFix = @"{
                    '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                    'EnumType': 'ETrailWidthMode(0)',
                    'Name': 'WidthMode(0)',
                    'DuplicationIndex': 0,
                    'Value': 'ETrailWidthMode_FromCentre(0)'
                  }";
                  
        public const string SkeletalAccessoryModelDataTableItem = @"{
                '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                'ID': 0,
                'Name': 'MeshData(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                'EnumType': 'EAttachAccessorySkeleton(0)',
                'Name': 'TargetSkeleton(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bCastShadow(0)',
                'DuplicationIndex': 0,
                'Value': true
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName1(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName2(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName3(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName4(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName5(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName6(0)',
                'DuplicationIndex': 0,
                'Value': null
              }
            ]
          }";

        public const string AttachAccessoryModelDataTableItem = @"{
                '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                'ID': 0,
                'Name': 'MeshData(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.EnumPropertyData, UAssetAPI',
                'EnumType': 'EAttachAccessorySkeleton(0)',
                'Name': 'AttachSkeleton(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                'ID': 0,
                'Name': 'AnimBlueprint(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                'ID': 0,
                'Name': 'GmPhysicsAsset(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)';
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bTurnOffEyebrowL(0)',
                'DuplicationIndex': 0,
                'Value': false
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bTurnOffEyebrowR(0)',
                'DuplicationIndex': 0,
                'Value': false
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bTurnOffEyelashL(0)',
                'DuplicationIndex': 0,
                'Value': false
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bTurnOffEyelashR(0)',
                'DuplicationIndex': 0,
                'Value': false
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bCastShadow(0)',
                'DuplicationIndex': 0,
                'Value': true
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.IntPropertyData, UAssetAPI',
                'Name': 'TranslucentSortPriority(0)',
                'DuplicationIndex': 0,
                'Value': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bPhysicsBodyHit(0)',
                'DuplicationIndex': 0,
                'Value': true
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName1(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName2(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName3(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName4(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName5(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                'Name': 'ReplacementMaterianName6(0)',
                'DuplicationIndex': 0,
                'Value': null
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'ReplacementTextureData(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'ReplacementTexture1(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                    'ID': 0,
                    'Name': 'TextureData(0)',
                    'DuplicationIndex': 0,
                    'Value': 'None(0)'
                  },
                  {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'TargetTextureName(0)',
                    'DuplicationIndex': 0,
                    'Value': null
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'ReplacementTextureData(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'ReplacementTexture2(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                    'ID': 0,
                    'Name': 'TextureData(0)',
                    'DuplicationIndex': 0,
                    'Value': 'None(0)'
                  },
                  {
                    '$type': 'UAssetAPI.PropertyTypes.StrPropertyData, UAssetAPI',
                    'Name': 'TargetTextureName(0)',
                    'DuplicationIndex': 0,
                    'Value': null
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.BoolPropertyData, UAssetAPI',
                'Name': 'bDefaultVisibility(0)',
                'DuplicationIndex': 0,
                'Value': true
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.IntPropertyData, UAssetAPI',
                'Name': 'GimmickChannel(0)',
                'DuplicationIndex': 0,
                'Value': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.SoftObjectPropertyData, UAssetAPI',
                'ID': 0,
                'Name': 'AnimData(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              }
            ]
          }";
        
        public const string CharacterCostume_DataTableItem = @"{
            '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
            'StructType': 'CharacterCostumeTableRow(0)',
            'SerializeNone': true,
            'StructGUID': '00000000-0000-0000-0000-000000000000',
            'Name': '',
            'DuplicationIndex': 0,
            'Value': [
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'CharacterBaseID(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'CostumeID(0)',
                'DuplicationIndex': 0,
                'Value': ''
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'SkeletalAccessoryID1(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'SkeletalAccessoryID2(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'SkeletalAccessoryID3(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID1(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone1(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos1(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos1(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot1(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot1(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale1(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID2(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone2(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos2(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos2(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot2(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot2(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale2(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID3(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone3(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos3(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos3(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot3(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot3(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale3(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID4(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone4(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos4(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos4(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot4(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot4(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale4(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID5(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone5(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos5(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos5(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot5(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot5(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale5(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AttachAccessoryID6(0)',
                'DuplicationIndex': 0,
                'Value': 'AAcc_acce_02_cos001_a(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'AccessoryAttachBone6(0)',
                'DuplicationIndex': 0,
                'Value': 'AT_acce_L(0)'
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachPos6(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachPos6(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': '+0',
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.StructTypes.StructPropertyData, UAssetAPI',
                'StructType': 'Vector(0)',
                'SerializeNone': true,
                'StructGUID': '00000000-0000-0000-0000-000000000000',
                'Name': 'AccessoryAttachRot6(0)',
                'DuplicationIndex': 0,
                'Value': [
                  {
                    '$type': 'UAssetAPI.StructTypes.VectorPropertyData, UAssetAPI',
                    'Name': 'AccessoryAttachRot6(0)',
                    'DuplicationIndex': 0,
                    'Value': {
                      '$type': 'UAssetAPI.FVector, UAssetAPI',
                      'X': -90.0,
                      'Y': '+0',
                      'Z': '+0'
                    }
                  }
                ]
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.FloatPropertyData, UAssetAPI',
                'Value': 1.0,
                'Name': 'AccessoryScale6(0)',
                'DuplicationIndex': 0
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'HideHairBone1(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'HideHairBone2(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              },
              {
                '$type': 'UAssetAPI.PropertyTypes.NamePropertyData, UAssetAPI',
                'Name': 'HideHairBone3(0)',
                'DuplicationIndex': 0,
                'Value': 'None(0)'
              }
            ]
          }";
    }
}