using System.Threading;
using System;
using System.IO;

using Gtk;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UI = Gtk.Builder.ObjectAttribute;

namespace CustomSlots
{
    public class MainWindow : Window
    {
        [UI] private Button _gamePaksFolderButton = null;
        [UI] private Entry _gamePaksFolderEntry = null;
        [UI] private TextView _outputLog = null;
        [UI] public TextBuffer _textBuffer = null;
        [UI] public ScrolledWindow _scrolledWindow = null;
        [UI] private Button _startButton = null;

        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);

            DeleteEvent += Window_DeleteEvent;
            _startButton.Clicked += StartButton_Clicked;
            _gamePaksFolderButton.Clicked += FolderButton_Clicked;
            _outputLog.SizeAllocated += Scroll_Down;

            LoadLastFolder();
        }

        private void SaveLastFolder(string path)
        {
            var last = new JObject(new JProperty("paks_folder", path));

            using (StreamWriter file = File.CreateText("last.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                last.WriteTo(writer);
            }
        }

        private void LoadLastFolder()
        {
            var fileName = "last.json";
            if (File.Exists(fileName))
            {
                using (StreamReader file = File.OpenText("last.json"))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    var jobj = (JObject) JToken.ReadFrom(reader);
                    _gamePaksFolderEntry.Text = jobj["paks_folder"].ToString();
                }
            }
        }

        private void FolderButton_Clicked(object sender, EventArgs a)
        {
            var filepicker = new FileChooserNative("Select folder", this, FileChooserAction.SelectFolder, "Open", "Cancel");
            if (filepicker.Run() == (int) ResponseType.Accept) 
            {
                _gamePaksFolderEntry.Text = filepicker.Filename;
            }

            filepicker.Destroy();
            SaveLastFolder(_gamePaksFolderEntry.Text);
        }

        private void StartButton_Clicked(object sender, EventArgs a)
        {
            _startButton.Sensitive = false;
            Thread thread = new Thread(() => SafeExecute(() => CustomSlots.Rebuild(_gamePaksFolderEntry.Text), Handler));
            thread.Start();
        }

        private void Scroll_Down(object sender, EventArgs a)
        {
            var adj = _scrolledWindow.Vadjustment;
            adj.Value = (adj.Upper - adj.PageSize);
        }

        public void setOutputText(string text) 
        {
            _textBuffer.Text += text+"\n";
        }

        private void Handler(Exception exception)
        {        
            CustomSlots.updateOutput(exception.Message);

            var currentPath = AppContext.BaseDirectory;
            var extractedFolder = System.IO.Path.Combine(currentPath, "extracted");

            if (Directory.Exists(extractedFolder))
            {
                Directory.Delete(extractedFolder, true);
            }
            
            CleanUP();
            _startButton.Sensitive = true;
        }

        private void SafeExecute(System.Action act, Action<Exception> handler)
        {
            try
            {
                act.Invoke();
                _startButton.Sensitive = true;
                CleanUP();
            }
            catch (Exception ex)
            {
                Handler(ex);
            }
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private void CleanUP()
        {
            foreach (var folder in Program.strayFolders)
            {
                Directory.Delete(folder, true);
            }
            Program.strayFolders.Clear();
        }
    }
}
