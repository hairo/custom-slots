using System.Collections.Generic;

namespace CustomSlots
{
    class PakFiles
    {
        public const string CostumeModelDataTableFile = "/StarlitSeason/Content/MD/CharacterModel/CostumeModelDataTable";
        public const string SkeletalAccessoryModelDataTableFile = "/StarlitSeason/Content/MD/CharacterModel/SkeletalAccessoryModelDataTable";
        public const string AttachAccessoryModelDataTableFile = "/StarlitSeason/Content/MD/CharacterModel/AttachAccessoryModelDataTable";
        public const string CharacterModelDataTableFile = "/StarlitSeason/Content/MD/CharacterModel/CharacterModelDataTable";
        public const string MD_CostumeModelFile = "/StarlitSeason/Content/MD/CharacterModel/MD_CostumeModel";
        public const string MD_ItemCostumeFile = "/StarlitSeason/Content/MD/Item/MD_ItemCostume";
        public const string MD_DLCUnlockConditionSteamFile = "/StarlitSeason/Content/MD/DLC/MD_DLCUnlockConditionSteam";

        public const string AssetRegistry = "/StarlitSeason/AssetRegistry";
        
        
        public static readonly List<string> CharacterCostumeFolder
                    = new List<string> { "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0001_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0101_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0102_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0103_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0104_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0105_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0106_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0107_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0108_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0109_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0110_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0111_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0112_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0113_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0201_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0202_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0203_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0204_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0205_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0206_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0301_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0302_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0303_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0304_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0305_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0306_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0401_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0402_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0403_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0404_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0405_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0406_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0501_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0502_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0601_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr0602_DataTable",
                                         "StarlitSeason/Content/MD/CharacterModel/CharacterCostume/CharacterCostume_chr9999_DataTable" 
                                       };

        public static List<string> GetAllFiles() 
        {
            var allFiles = new List<string> 
            {
                CostumeModelDataTableFile,
                // SkeletalAccessoryModelDataTableFile,
                AttachAccessoryModelDataTableFile,
                CharacterModelDataTableFile,
                MD_CostumeModelFile,
                MD_ItemCostumeFile,
                MD_DLCUnlockConditionSteamFile,
                AssetRegistry
            };

            allFiles.AddRange(CharacterCostumeFolder);

            return allFiles;
        }
    }
}